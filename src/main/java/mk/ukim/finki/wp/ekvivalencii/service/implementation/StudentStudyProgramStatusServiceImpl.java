package mk.ukim.finki.wp.ekvivalencii.service.implementation;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.ekvivalencii.model.Student;
import mk.ukim.finki.wp.ekvivalencii.model.StudentEquivalenceRequest;
import mk.ukim.finki.wp.ekvivalencii.model.StudentGrade;
import mk.ukim.finki.wp.ekvivalencii.model.StudyProgramSubject;
import mk.ukim.finki.wp.ekvivalencii.model.exceptions.StudentNotFoundException;
import mk.ukim.finki.wp.ekvivalencii.repository.StudyProgramSubjectRepository;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.EquivalentSubjectService;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.StudentGradesService;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.StudentService;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.StudentStudyProgramStatusService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class StudentStudyProgramStatusServiceImpl implements StudentStudyProgramStatusService {
    private final StudentService studentService;
    private final StudentGradesService studentGradesService;
    private final StudyProgramSubjectRepository studyProgramSubjectRepository;
    private final EquivalentSubjectService equivalentSubjectService;

    @Override
    public Map<StudyProgramSubject, Optional<StudentGrade>> getStudentStudyProgramStatus(String studentIndex) {

        Student student = studentService.getStudentById(studentIndex).orElseThrow(StudentNotFoundException::new);
        List<StudyProgramSubject> studyProgramSubjects = studyProgramSubjectRepository.findByStudyProgramCodeOrderBySemesterAscOrderAsc(student.getStudyProgram().getCode());
        return studyProgramSubjects.stream().map(it -> {
            Optional<StudentGrade> grade = studentGradesService.findById(String.format("%s_%s", student.getIndex(), it.getSubject().getId()));
            return Map.entry(it, grade);
        }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public Map<StudyProgramSubject, Optional<StudentGrade>> getStudentStudyProgramEquivalence(String studentIndex, String studyProgramCode) {
        return Map.of();
    }

    public Map<StudyProgramSubject, Optional<StudentGrade>> mapGrades(StudentEquivalenceRequest request) {
        String studentIndex = request.getStudent().getIndex();
        String studyProgramCode = request.getNewStudyProgram().getCode();
        Student student = studentService.getStudentById(studentIndex).orElseThrow(StudentNotFoundException::new);
        // grades as map with subject id as key
        Map<String, StudentGrade> studentGrades = studentGradesService.findAllStudentGrades(studentIndex)
                .stream()
                .collect(Collectors.toMap(it -> it.getSubject().getId(), it -> it));

        return studyProgramSubjectRepository.findByStudyProgramCodeOrderBySemesterAscOrderAsc(studyProgramCode).stream().map(it -> {
            String subjectId = it.getSubject().getId();
            List<String> equivalentCodes = equivalentSubjectService.findEquivalentSubjectCodes(subjectId);
            Optional<StudentGrade> grade = Optional.empty();
            for (String code : equivalentCodes) {
                if (studentGrades.containsKey(code)) {
                    grade = Optional.of(studentGrades.get(code));
                    break;
                }
            }
            return Map.entry(it, grade);
        }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
