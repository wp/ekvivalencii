package mk.ukim.finki.wp.ekvivalencii.model;

public enum EquivalenceStatus {

    REQUESTED, SUBJECTS_IMPORTED, EQUIVALENCE_IN_PROGRESS, FINISHED
}
