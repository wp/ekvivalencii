package mk.ukim.finki.wp.ekvivalencii.service.interfaces;

import mk.ukim.finki.wp.ekvivalencii.model.DTO.StudentGradesDto;
import mk.ukim.finki.wp.ekvivalencii.model.Student;
import mk.ukim.finki.wp.ekvivalencii.model.StudentGrade;
import mk.ukim.finki.wp.ekvivalencii.model.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface StudentGradesService {
    Page<StudentGrade> list(Specification<StudentGrade> spec, int page, int size);

    Optional<StudentGrade> save(Student student, Subject subject, LocalDateTime gradeDate, Short grade, String ectsGrade);

    Optional<StudentGrade> edit(String id, Student student, Subject subject, LocalDateTime gradeDate, Short grade, String ectsGrade);

    Optional<StudentGrade> findById(String id);

    void deleteById(String id);

    List<StudentGrade> findAllStudentGrades(String studentIndex);

    StudentGrade saveStudentGrades(StudentGrade studentGrades);

    List<StudentGradesDto> importGradesFromCsv(List<StudentGradesDto> importStudentGrades);

    StudentGrade saveStudentDto(Subject subject,
                                Student student,
                                String gradeDate,
                                Short grade,
                                String ectsGrade);

    Page<StudentGrade> filterAndPaginateStudentGrades(String studentIndex, int pageNum, int pageSize);
}
