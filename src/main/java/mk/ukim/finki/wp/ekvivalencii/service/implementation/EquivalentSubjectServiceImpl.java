package mk.ukim.finki.wp.ekvivalencii.service.implementation;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.ekvivalencii.model.Subject;
import mk.ukim.finki.wp.ekvivalencii.repository.JoinedSubjectRepository;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.EquivalentSubjectService;
import org.springframework.stereotype.Service;

import java.util.*;

@AllArgsConstructor
@Service
public class EquivalentSubjectServiceImpl implements EquivalentSubjectService {

    final Map<String, HashSet<String>> subjectMapping = new HashMap<>();
    private final JoinedSubjectRepository joinedSubjectRepository;

    @PostConstruct
    public void init() {
        joinedSubjectRepository.findAll().forEach(it -> {

            Set<String> codes = new HashSet<>(List.of(it.getCodes().replaceAll(" ", "").split(";")));
            for (String code : codes) {
                HashSet<String> result = new HashSet<>(subjectMapping.getOrDefault(code, new HashSet<>()));
                result.addAll(codes);
                subjectMapping.put(code, result);
            }
        });
    }


    @Override
    public List<String> findEquivalentSubjectCodes(String subjectId) {
        return subjectMapping.getOrDefault(subjectId, new HashSet<>()).stream().toList();
    }
}
