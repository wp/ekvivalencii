package mk.ukim.finki.wp.ekvivalencii.repository;


import mk.ukim.finki.wp.ekvivalencii.model.StudyProgramSubject;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudyProgramSubjectRepository extends JpaSpecificationRepository<StudyProgramSubject, String> {

    List<StudyProgramSubject> findByStudyProgramCodeOrderBySemesterAscOrderAsc(String studyProgramCode);

}
