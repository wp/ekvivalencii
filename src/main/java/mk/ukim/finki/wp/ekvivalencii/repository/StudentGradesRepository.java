package mk.ukim.finki.wp.ekvivalencii.repository;

import mk.ukim.finki.wp.ekvivalencii.model.Student;
import mk.ukim.finki.wp.ekvivalencii.model.StudentGrade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentGradesRepository extends JpaSpecificationRepository<StudentGrade, String> {
    Page<StudentGrade> findByStudent(Optional<Student> student, Pageable pageable);

    List<StudentGrade> findByStudentIndex(String studentIndex);

}
