package mk.ukim.finki.wp.ekvivalencii.service.interfaces;

import mk.ukim.finki.wp.ekvivalencii.model.Subject;

import java.util.List;

public interface EquivalentSubjectService {

    List<String> findEquivalentSubjectCodes(String subjectId);
}
