package mk.ukim.finki.wp.ekvivalencii.repository;

import mk.ukim.finki.wp.ekvivalencii.model.JoinedSubject;
import org.springframework.stereotype.Repository;

@Repository
public interface JoinedSubjectRepository extends JpaSpecificationRepository<JoinedSubject, String> {

}
