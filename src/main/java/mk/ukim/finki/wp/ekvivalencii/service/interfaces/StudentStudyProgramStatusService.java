package mk.ukim.finki.wp.ekvivalencii.service.interfaces;

import mk.ukim.finki.wp.ekvivalencii.model.StudentGrade;
import mk.ukim.finki.wp.ekvivalencii.model.StudyProgramSubject;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface StudentStudyProgramStatusService {

    Map<StudyProgramSubject, Optional<StudentGrade>> getStudentStudyProgramStatus(String studentIndex);

    Map<StudyProgramSubject, Optional<StudentGrade>> getStudentStudyProgramEquivalence(String studentIndex, String studyProgramCode);

}
