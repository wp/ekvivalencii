package mk.ukim.finki.wp.ekvivalencii.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.wp.ekvivalencii.model.DTO.StudentGradesDto;
import mk.ukim.finki.wp.ekvivalencii.model.Student;
import mk.ukim.finki.wp.ekvivalencii.model.StudentGrade;
import mk.ukim.finki.wp.ekvivalencii.model.Subject;
import mk.ukim.finki.wp.ekvivalencii.model.exceptions.StudentNotFoundException;
import mk.ukim.finki.wp.ekvivalencii.repository.ImportRepository;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.StudentGradesService;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.StudentService;
import mk.ukim.finki.wp.ekvivalencii.service.interfaces.SubjectService;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static mk.ukim.finki.wp.ekvivalencii.service.interfaces.specifications.FieldFilterSpecification.filterEquals;
import static mk.ukim.finki.wp.ekvivalencii.service.specifications.FieldFilterSpecification.filterEqualsV;

@Controller
public class StudentGradesImportController {
    private final StudentGradesService studentGradesService;
    private final StudentService studentService;
    private final ImportRepository importRepository;
    private final SubjectService subjectService;

    public StudentGradesImportController(StudentGradesService studentGradesService, StudentService studentService, ImportRepository importRepository, SubjectService subjectService) {
        this.studentGradesService = studentGradesService;
        this.studentService = studentService;
        this.importRepository = importRepository;
        this.subjectService = subjectService;
    }

    @GetMapping("/student-grades/{id}")
    public String getStudentGrades(@PathVariable String id, @ModelAttribute StudentGrade studentGrade, @RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "10") Integer results, @RequestParam(required = false) String index, @RequestParam(required = false) String subjectId, Model model) {

        Specification<StudentGrade> spec = Specification.where(filterEquals(StudentGrade.class, "student.index", index));

        if (subjectId != null && !subjectId.isEmpty()) {
            spec = spec.and(filterEqualsV(StudentGrade.class, "subject.id", subjectId));
        }
        Page<StudentGrade> studentGradesPage = studentGradesService.list(spec, pageNum, results);
        model.addAttribute("page", studentGradesPage);

        List<Subject> subjects = subjectService.getAllSubjects();
        model.addAttribute("subjects", subjects);
        model.addAttribute("id", id);
        return "list";
    }

    @GetMapping("/student-grades/{id}/add")
    public String showAddStudentGradeForm(@PathVariable String id, Model model) {
        List<Student> students = studentService.getAllStudents();
        List<Subject> subjects = subjectService.getAllSubjects();

        model.addAttribute("studentGrade", new StudentGrade());
        model.addAttribute("students", students);
        model.addAttribute("subjects", subjects);
        return "add";
    }

    @ExceptionHandler
    public String handleNoSuchElementException(StudentNotFoundException exception, Model model) {
        model.addAttribute("message", exception.getMessage());
        return "errorPage";
    }

    @PostMapping("/student-grades/{id}/save")
    public String addStudentGrade(@PathVariable(required = false) String id, @RequestParam(required = false) Student student, @RequestParam(required = false) Subject subject, @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime gradeDate, @RequestParam(required = false) Short grade, @RequestParam(required = false) String ectsGrade) {
        if (student == null || student.getIndex().isEmpty()) {
            throw new StudentNotFoundException();
        }
        StudentGrade grades = new StudentGrade();
        String newId = String.format("%s_%s", student.getIndex(), subject.getId());
        grades.setId(newId);
        grades.setStudent(student);
        grades.setSubject(subject);
        grades.setGradeDate(gradeDate);
        grades.setGrade(grade);
        grades.setEctsGrade(ectsGrade);
        this.studentGradesService.saveStudentGrades(grades);
        return "redirect:/student-grades/{id}";
    }

    @PostMapping("/student-grades/{id}/delete")
    public String deleteSubject(@PathVariable String id) {
        this.studentGradesService.deleteById(id);
        return "redirect:/student-grades/{id}";
    }

    @GetMapping("/student-grades/{id}/edit")
    public String editStudentGrades(@PathVariable String id, Model model) {
        if (this.studentGradesService.findById(id).isPresent()) {
            StudentGrade studentGrades = this.studentGradesService.findById(id).get();
            List<Student> students = this.studentService.getAllStudents();
            List<Subject> subjects = this.subjectService.getAllSubjects();

            model.addAttribute("students", students);
            model.addAttribute("subjects", subjects);
            model.addAttribute("studentGrades", studentGrades);
            return "edit";
        }
        return "/student-grades/{id}";
    }

    @PostMapping("/student-grades/{id}/edit")
    public String editStudentGrades(@PathVariable String id, @RequestParam Student student, @RequestParam Subject subject, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime gradeDate, @RequestParam Short grade, @RequestParam String ectsGrade) {
        this.studentGradesService.edit(id, student, subject, gradeDate, grade, ectsGrade);
        return "redirect:/student-grades/{id}";
    }

    @PostMapping("/student-grades/{id}/import")
    public String importCourseStudentGrades(@PathVariable String id, @RequestParam("file") MultipartFile file, HttpServletResponse response, Model model) {
        try {
            List<StudentGradesDto> grades = importRepository.readFile(file, StudentGradesDto.class);
            List<StudentGradesDto> invalidGrades = studentGradesService.importGradesFromCsv(grades);

            if (!invalidGrades.isEmpty()) {
                String fileName = "invalid_grades_" + ".tsv";
                response.setContentType("text/tab-separated-values");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                try (OutputStream outputStream = response.getOutputStream()) {
                    importRepository.writePreferences(StudentGradesDto.class, invalidGrades, outputStream);
                }
            }

            return "redirect:/student-grades/" + id;
        } catch (IOException e) {
            model.addAttribute("error", "An error occurred while processing the file.");
            return "errorPage";
        }
    }

    @GetMapping("/student-grades/{id}/sample-tsv")
    public void sampleTsv(@PathVariable String id, HttpServletResponse response, Model model) {
        List<StudentGradesDto> example = new ArrayList<>();
        example.add(new StudentGradesDto("F23L2W096", "1", "2023-04-10T10:39:37", (short) 8, "72", null));
        example.add(new StudentGradesDto("F23L3W074", "1", "2023-04-10T10:39:37", (short) 10, "7", null));

        doExport(response, example);
        model.addAttribute("id", id);
    }

    @GetMapping("/student-grades/{id}/export")
    public void export(Model model, @PathVariable String id, @RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "100") Integer results, @RequestParam(required = false) String index, @RequestParam(required = false) String subjectId, HttpServletResponse response) {

        Page<StudentGrade> studentGradesPage;

        Specification<StudentGrade> spec = Specification.where(filterEquals(StudentGrade.class, "student.index", index));

        if (subjectId != null && !subjectId.isEmpty()) {
            spec = spec.and(filterEqualsV(StudentGrade.class, "subject.id", subjectId));
        }

        studentGradesPage = studentGradesService.list(spec, pageNum, results);

        doExport(response, studentGradesPage.getContent().stream().map(sg -> new StudentGradesDto(sg.getSubject().getId(), sg.getStudent().getIndex(), sg.getGradeDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), sg.getGrade(), sg.getEctsGrade(), null)).collect(Collectors.toList()));

        model.addAttribute("id", id);
    }

    public void doExport(HttpServletResponse response, List<StudentGradesDto> data) {
        String fileName = "student_grades.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeEnrollments(StudentGradesDto.class, data, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

